<?php $role= array (
array("name" => ".xl","media" => "all"),
array("name" => ".lg","media" => "(max-width:992px)"),
array("name" => ".md","media" => "(max-width:768px)"),
array("name" => ".sm","media" => "(max-width:576px)"),
array("name" => ".xs","media" => "(max-width:280px)"),			
);$position = array('','-top','-bottom','-left','-right');ob_start(); ?>


br {
float:none !important; 
clear:both !important;}

.group {  
writing-mode: horizontal-tb;
display:-webkit-box;
display:-webkit-flex;
display:-webkit-flexbox;
display:-moz-flex;
display:-moz-box;
display:-ms-flexbox;
display:flex;
flex-flow: row wrap;
flex-direction:row wrap;
flex-flow: row wrap;
-webkit-flex-wrap:wrap;
-ms-flex-wrap:wrap;
flex-wrap:wrap;
-ms-flex: 0 0 0px;
flex: 0 0 0px;
-ms-flex-preferred-size: auto;
flex-basis:auto;
word-break:break-strict;
width: auto;
clear: both;
float: none;

}

.group 	 > *{ 
 
writing-mode: horizontal-tb;
-ms-flex: 1 1 auto;
flex: 1 1 auto;
flex-direction: row wrap;
flex-flow: row wrap;
-webkit-flex-wrap:   wrap; 
-ms-flex-wrap:   wrap;
flex-wrap:   wrap;
-ms-flex-preferred-size: auto;
flex-basis: auto;
word-break: break-strict;
-ms-flex-item-align: flex-start;
 position: relative;
clear: both;
float: none;
 
  }
.group 	 > div >*{padding:10px; }

 
 
 .group 	 > *{box-shadow:  inset 0 0 2px #bbb;
 -moz-box-shadow: inset 0 0 2px #bbb;
  -ms-box-shadow:inset 0 0 2px #bbb;
  -webkit-box-shadow: inset 0 0 2px #bbb;}	
 
 
*{text-decoration:none !important; } 								
@media \0screen\,screen\9{*{overflow: visible}}
@media screen and (min-width:0\0){*{overflow: visible}}	
@media (max-width:768px) {.group 	 > .auto{ width: calc(100% / 24 * 24)!important ;} }

<?php /*?>DEFAULT--------------------------------------------------------<?php */?><?php 
foreach ($role as $data) 					{   ?>	 
@media <?= $data["media"]  ?>{	

<?=$data["name"]?>-noview			{display: none !important;}
<?=$data["name"]?>-onview			{display: inline !important;}

<?=$data["name"]?>-onbreak				{word-break:break-all!important ;}
<?=$data["name"]?>-nobreak				{word-break: break-strict!important ;}

<?=$data["name"]?>-onwrap 				{white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}			
<?= $data["name"]?>-nowrap 				{white-space: inherit;overflow: inherit;text-overflow: ellipsis;position:relative;display:flex;}

<?=$data["name"]?>-onflat				{white-space: nowrap;flex-flow: row ;}
<?=$data["name"]?>-noflat				{white-space: normal;flex-direction: row wrap;flex-flow: row wrap;}

<?=$data["name"]?>-onfix				{-ms-flex: 0 1 auto;flex: 0 1 auto!important ;}
<?=$data["name"]?>-nofix				{-ms-flex: 1 1 auto;flex:1 1 auto!important ; }
									
<?=$data["name"]?>-justify			{text-align: justify!important ;}
<?=$data["name"]?>-center			{text-align: center!important ;}
<?=$data["name"]?>-left				{text-align: left!important ;}
<?=$data["name"]?>-right				{text-align: right!important ;}

<?=$data["name"]?>-self-middle{
display: -ms-flexbox;
display: -webkit-flex;
display: flex;
-webkit-flex-direction: row;
-ms-flex-direction: row;
flex-direction: row;
-webkit-flex-wrap: nowrap;
-ms-flex-wrap: nowrap;
flex-wrap: nowrap;
/*-ms-flex-pack: start;
*/-webkit-align-content: stretch;
-ms-flex-line-pack: stretch;
align-content: stretch;
-webkit-align-items: center;
-ms-flex-align: center;
align-items: center; height:100%;
margin-bottom: auto; 
margin-top:auto;}	
																			
<?=$data["name"]?>-self-top{
display: -ms-flexbox;display: -webkit-flex;display: flex;-webkit-align-content: flex-start;align-content: flex-start;-webkit-align-items: flex-start;align-items: flex-start; height:100%;}									

<?=$data["name"]?>-self-bottom{
display: -ms-flexbox;display: -webkit-flex;display: flex;-webkit-align-content: flex-end;align-content: flex-end;-webkit-align-items: flex-end;align-items: flex-end; height:100%;}									

<?=$data["name"]?>-self-center{
display: -webkit-box ;display: -webkit-flex;display: -ms-flexbox;display: flex; margin: auto;align-items: center;align-content: center;justify-content: center; -ms-flex-item-align: center; }									

<?=$data["name"]?>-self-left{display: -webkit-box ;display: -webkit-flex;display: -ms-flexbox;display: flex;align-content:flex-start; align-items:flex-start; align-self:flex-start;}											

<?=$data["name"]?>-self-right{display: -webkit-box ;display: -webkit-flex;display: -ms-flexbox;display: flex;align-content:flex-end; align-items:flex-end; align-self:flex-end;    
  justify-content: flex-end;}	 									

<?=$data["name"]?>-self-central{
display: -webkit-box ;display: -webkit-flex;display: -ms-flexbox;display: flex; height:100%;margin: auto;align-items: center;align-content: center;justify-content: center; -ms-flex-item-align: center;}} 
<?php }?>


<?php /*?>WIDTH--------------------------------------------------------<?php */?><?php 	
foreach ($role as $data) 					{	?>	
@media <?= $data["media"]  ?>{ 	
<?php for($i=0; $i <25; ++$i){
echo $data["name"].'-width-'.$i.'{width: calc(100% / 24 * '.$i.') !important ;}';}?>}
<?php }?>

<?php /*?>ORDER--------------------------------------------------------<?php */?><?php 
foreach ($role as $data){	?>	
@media <?= $data["media"]  ?>{ 	
<?=$data["name"]?>-order-0, *{order:11;}
<?php for($i=0; $i <11; ++$i){	echo $data["name"].'-order-'.$i.'	{ order: '.$i.'!important ;}';}?>}
<?php }?>


<?php /*?>DONE--------------------------------------------------------<?php */?><?php 
$ob_get= ob_get_contents();ob_end_clean();
$ob_get = preg_replace('/\s+/',' ',$ob_get);
header('Content-Type: text/css');echo $ob_get ; ?> 





  